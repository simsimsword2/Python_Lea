# ----------------------------------------------------------------------------------------------------------------------
# Node:
# Combination of value and pointer to another node
class Node:
    def __init__(self, value=None):
        self.value = value
        self.nextNode = None

    def __str__(self):
        return self.value

# ----------------------------------------------------------------------------------------------------------------------
# Linked list:
# A chain of Nodes
class LinkedList:
#   Creating an empty list
    def __init__(self):
        self.firstNode = None
        self.listLength = 0

    def addNode(self, node):
        if self.firstNode is None:
            self.firstNode = node
            self.listLength = 1
        else:
            checkNode = self.firstNode

            while checkNode.nextNode is not None:
                checkNode = checkNode.nextNode

            checkNode.nextNode = node
            self.listLength += 1

    def printList(self):
        printNode = self.firstNode
        listCount = 1

        while printNode is not None:
            print (str(listCount) + ': ' + printNode.value)
            printNode = printNode.nextNode
            listCount += 1

    def reverse(self):
        if self.firstNode is not None and self.firstNode.nextNode is not None:
            previousNode = self.firstNode
            currentNode = self.firstNode.nextNode

            while currentNode.nextNode is not None:
                tempNode = currentNode.nextNode
                currentNode.nextNode = previousNode
                previousNode = currentNode
                currentNode = tempNode

            currentNode.nextNode = previousNode
            self.firstNode.nextNode = None
            self.firstNode = currentNode