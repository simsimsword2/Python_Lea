# 16.04.2023
#
# linked list in Python
# ----------------------------------------------------------------------------------------------------------------------
# import:
import LinkedList as lList
# ----------------------------------------------------------------------------------------------------------------------
# setup:

n1 = lList.Node('simon')
n2 = lList.Node('ambuehl')
n3 = lList.Node('hat')
n4 = lList.Node('keine')
n5 = lList.Node('Ahnung')
n6 = lList.Node('was')
n7 = lList.Node('los')


ll1 = lList.LinkedList()

ll1.addNode(n1)
ll1.addNode(n2)
ll1.addNode(n3)
ll1.addNode(n4)
ll1.addNode(n5)
ll1.addNode(n6)
ll1.addNode(n7)

ll1.printList()
print('Length of the list: ' + str(ll1.listLength))

ll1.reverse()
ll1.printList()
print('Length of the list: ' + str(ll1.listLength))