# 23.03.2021
#
# Schreiben Sie den Bubble-Sort-Algorithmus aus dem letzten Modul mit zwei Funktionen z.B. bubblesort() und swap().

import Modules
import random
import time

# ----------------------------------------------------------------------------------------------------------------------
# setup:

a = [5, 1, 3, 2, 4]
random_list = []
sort_baseline = []
sort_list_bubble = []
sort_list_sim = []

# print(a)
# Modules.bubblesort(a)
# print(a)

for i in range(0, 1000):
    n = random.randint(1, 1000)
    random_list.append(n)

# ----------------------------------------------------------------------------------------------------------------------
# Python's own sorting

sort_baseline.extend(random_list)
start = time.time()

# print(random_list)
sort_baseline.sort()
# print(random_list)

end = time.time()
print("Sorting the list with python's own soring                   ", end - start, "s")

# ----------------------------------------------------------------------------------------------------------------------
# bubble sort:

sort_list_bubble.extend(random_list)

start = time.time()

# print(random_list)
print("Itartatons: " + str(Modules.bubblesort(sort_list_bubble)))
# print(random_list)

end = time.time()
print("Das Sortieren der zufaelligen Liste mit bubble sort dauerte ", end - start, "s")

# check is both results are the same
if sort_baseline != sort_list_bubble:
    print('Bubble sorting is wrong')

# ----------------------------------------------------------------------------------------------------------------------
# sim sort

sort_list_sim.extend(random_list)
start = time.time()

# print(random_list)
print("Itarations: " + str(Modules.simsort(sort_list_sim)))
# print(random_list)

end = time.time()
print("Sorting the list with simsort took                          ", end - start, "s")


# check is both results are the same
if sort_baseline != sort_list_sim:
    print('sim sorting is wrong')
